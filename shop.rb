%w{cart line_item product product_combination product_variation product_variation_term}.each do |v|
  require_relative "lib/#{v}"
end

# Instância do produto
camisa = Product.new('Camisa', 2, 20)

# Instância da variação e dos termos dela
cor = ProductVariation.new('Cor')
cor_azul = ProductVariationTerm.new('Azul', cor)
cor_vermelho = ProductVariationTerm.new('Vermelho', cor)

# Instância das combinações envolvendo o produto e os termos
camisa_combinacao1 = ProductCombination.new(camisa, 2, cor_azul)

# Procurar as combinações que contenham a cor azul nos termos,
# só que abaixo não funciona...
puts camisa.find_combination(cor_azul).inspect

# mas isso funciona...
puts camisa_combinacao1.terms.include?(cor_azul).inspect