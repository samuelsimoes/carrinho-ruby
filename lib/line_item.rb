class LineItem
  attr_accessor :cart

  def initialize(cart, product, qtd, variation=nil)
    @cart = cart
    @product = product
    @qtd = qtd
    @variation = variation
    @cart.products << self
  end

  def sum
    @product.price * @qtd
  end
end