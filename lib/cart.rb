class Cart
  attr_accessor :products

  def initialize
    @products = []
  end

  def total_sum
    total_sum = 0
    @products.each do |p|
      total_sum += p.sum
    end
    total_sum
  end
end