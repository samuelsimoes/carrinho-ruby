class ProductVariation
  attr_accessor :name, :terms

  def initialize(name=nil)
    @name = name
    @terms = []
  end
end