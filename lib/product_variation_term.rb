class ProductVariationTerm
  attr_accessor :variation_group, :name

  def initialize(name=nil, variation_group)
    @name = name
    @variation_group = variation_group
    @variation_group.terms << self
  end
end

class ProductCombination
  attr_accessor :terms, :stock, :parent_product
  def initialize(product, stock, *terms)
    @terms = terms
    @stock = stock
    @parent_product = product
    @parent_product.combinations << self
  end
end