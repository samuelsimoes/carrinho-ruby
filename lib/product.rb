class Product
  attr_accessor :price, :allowed_terms, :variations, :stock, :name, :terms, :combinations

  def initialize(name, stock, price)
    @price = price
    @stock = stock
    @name = name
    @variations = []
    @allowed_terms = []
    @combinations = []
  end

  def find_combination(*terms)
    @combinations.inject([]) do |result, combination|
      result << combination if terms.all? { |e| combination.terms.include?(e) }
      result
    end
  end

  def variations
    @allowed_terms.inject([]) do |result, term|
      result << term.variation_group.name
    end
  end
end