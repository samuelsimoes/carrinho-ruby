class ProductCombination
  attr_accessor :terms, :stock, :parent_product
  def initialize(product, stock, *terms)
    @terms = terms
    @stock = stock
    @parent_product = product
    @parent_product.combinations << self
  end
end